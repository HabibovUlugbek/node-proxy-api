const express = require('express')
const rateLimit = require('express-rate-limit')
require('dotenv').config()
const cors = require('cors')

const PORT = process.env.PORT || 5000 ;

const app = express();


//Rate limiting
const limiter = rateLimit({
    windowMs: 10 * 6 * 1000,
    max: 5
})
app.use(limiter)
app.set('trust proxy', 1)

//Static folder
app.use(express.static('public'))

//Routes
app.use("/api" , require("./routes"))


//Use cors
app.use(cors())

app.listen(PORT , ()=>{
    console.log(`Server running on ${PORT}`)
})